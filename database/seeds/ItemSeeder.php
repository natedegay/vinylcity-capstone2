<?php

use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
		'name' =>'Jazz & Blues'
		]);

		DB::table('items')->insert([
		'name' =>'Indie'
		]);

		DB::table('items')->insert([
		'name' =>'Hip-Hop'
		]);

		DB::table('items')->insert([
		'name' =>'Rock'
		]);
    }
}
