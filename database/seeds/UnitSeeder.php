<?php

use Illuminate\Database\Seeder;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('units')->insert([
		'name' =>'Aretha Now',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/aretha_now_vinyl_store_800x.jpg?v=1579894173',
		'item_id'=> 1, 
		'unit_status_id' => 2,
		'control_code' => 'JAZ'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Live in Paris 1960',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/Miles_Davis_John_Coltrane_the_final_tour_vinyl_800x.jpg?v=1564069252',
		'item_id'=> 1, 
		'unit_status_id' => 1,
		'control_code' => 'JAZ'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Straight From the Heart',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/straight_from_the_heart_vinyl_store_eb62ebc7-2f54-4921-be7f-307f1c9765a6_800x.jpg?v=1582589974',
		'item_id'=> 1, 
		'unit_status_id' => 1,
		'control_code' => 'JAZ'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Lucille',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/lucille_vinyl_store_b24bfe3f-c3ac-4e6b-9862-c972ca7f4397_800x.jpg?v=1574798606',
		'item_id'=> 1, 
		'unit_status_id' => 1,
		'control_code' => 'JAZ'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Jazz Impressions of Japan',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/jazz_impressions_of_japan_vinyl_store_800x.jpg?v=1573253707',
		'item_id'=> 1, 
		'unit_status_id' => 3,
		'control_code' => 'JAZ'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Magician',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/magician_vinyl_store_800x.jpg?v=1587572842',
		'item_id'=> 1, 
		'unit_status_id' => 1,
		'control_code' => 'JAZ'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>"The Center Won't Hold",
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/magician_vinyl_store_800x.jpg?v=1587572842',
		'item_id'=> 4, 
		'unit_status_id' => 1,
		'control_code' => 'ROC'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'The Soft Bulletin',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/the_soft_bulletin_vinyl_store_800x.jpg?v=1570718378',
		'item_id'=> 4, 
		'unit_status_id' => 1,
		'control_code' => 'ROC'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Let it Die',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/magician_vinyl_store_800x.jpg?v=1587572842',
		'item_id'=> 2, 
		'unit_status_id' => 1,
		'control_code' => 'IND'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Be the Cowboy',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/be_the_cowboy_vinyl_store_800x.jpg?v=1564071145',
		'item_id'=> 2, 
		'unit_status_id' => 1,
		'control_code' => 'IND'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Honest',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/honest_vinyl_store_800x.jpg?v=1590013584',
		'item_id'=> 3, 
		'unit_status_id' => 1,
		'control_code' => 'HIP'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'The Carter',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/tha_carter_iii_vinyl_store_800x.jpg?v=1564072868',
		'item_id'=> 3, 
		'unit_status_id' => 1,
		'control_code' => 'HIP'.'-'. strtoupper(Str::random(6))
		]);

		DB::table('units')->insert([
		'name' =>'Doggy Style',
		'image' => 'https://cdn.shopify.com/s/files/1/0069/3465/9162/products/doggystyle_vinyl_800x.jpg?v=1564069868',
		'item_id'=> 3, 
		'unit_status_id' => 1,
		'control_code' => 'HIP'.'-'. strtoupper(Str::random(6))
		]);



		
    }
}
