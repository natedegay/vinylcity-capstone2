<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UnitStatusSeeder::class,
            RequestStatusSeeder::class,
            RoleSeeder::class,
            UserSeeder::class,
         	ItemSeeder::class,
         	UnitSeeder::class
         ]);
    }
}
