<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Unit;
use App\requestStatus;
use Illuminate\Http\Request;
use Str;
use Auth;
use DB;
use Brian2694\Toastr\Facades\Toastr;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // filter tickets by request status_id
        $all_tickets = Auth::id() == 1 ? Ticket::all() : Ticket::where('user_id',Auth::id())->get();

        $approved_tickets = Auth::id() == 1 ? 
            Ticket::where('request_status_id',1)->get() :
            Ticket::where([
                    'request_status_id' => 1,
                    'user_id' => Auth::id()
                ])->get();

        $pending_tickets = Auth::id() == 1 ? 
            Ticket::where('request_status_id',2)->get() :
            Ticket::where([
                    'request_status_id' => 2,
                    'user_id' => Auth::id()
                ])->get();

        $declined_tickets = Auth::id() == 1 ? 
            Ticket::where('request_status_id',3)->get() :
            Ticket::where([
                    'request_status_id' => 3,
                    'user_id' => Auth::id()
                ])->get();

        $completed_tickets = Auth::id() == 1 ? 
            Ticket::where('request_status_id',4)->get() :
            Ticket::where([
                    'request_status_id' => 4,
                    'user_id' => Auth::id()
                ])->get();
        // dd(Ticket::where('user_id',2));

        return view('tickets.index')
            ->with('all_tickets',$all_tickets)
            ->with('approved_tickets',$approved_tickets)
            ->with('pending_tickets',$pending_tickets)
            ->with('declined_tickets',$declined_tickets)
            ->with('completed_tickets',$completed_tickets)
            ->with('request_statuses',requestStatus::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([ 
            'date_needed' => 'required|date',
            'date_return' => 'required|date|after:date_needed',

        ]);

        $ticket = new Ticket($validatedData);

        $ticket->ticket_code = strtoupper(Str::random(6));
        $ticket->user_id = Auth::user()->id;
        
        $ticket->save();

        // unit_ticket pivot table
        $units = Unit::find(array_keys(session('request')));

        foreach($units as $unit){
            $ticket->units()->attach($unit->id, [
                'ticket_id' => $ticket->id,
                'date_needed' => $ticket->date_needed,
                'date_return' => $ticket->date_return,
                'request_status_id' => 2
            ]);
        }

        $ticket->save();
        Toastr::info('Ticket created');
        // clear request after saving
        session()->forget('request');

        return redirect( route('tickets.show',$ticket->id))->with('ticket',$ticket);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket)
    {
        $this->authorize('view',$ticket);
        $request_statuses = requestStatus::all();

        return view('tickets.show')->with('ticket',$ticket)->with('request_statuses',$request_statuses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        // authenticate
        $this->authorize('update',Ticket::class);

        // update request_status from ticket table and save
        $ticket->request_status_id = $request->request_status_id;
        $ticket->save();

        // update request_status from unit_ticket pivot
        $unit_ticket = DB::table('unit_ticket')
            ->where('ticket_id',$request->id)
            ->update(['request_status_id' => $request->request_status_id]);

        Toastr::success('Ticket status updated successfully');

        if($request->request_status_id == 1) {

            // get all units with approved status
            $approved_units = DB::table('unit_ticket')
                ->where('ticket_id', $request->id)
                ->where('request_status_id',1)->get();

            // store now date to variable
            $now = strtotime("now");

            foreach($approved_units as $unit){
                // if unit is borrowed today/before today,
                if ( strtotime($unit->date_needed) <= $now){

                    // if unit is not yet returned today, change unit_status to unavailable
                    if ( $now <= strtotime($unit->date_return)){
                        DB::table('units')
                            ->where('id',$unit->unit_id)
                            ->update(['unit_status_id'=>2]);

                    // if unit has been returned, change ticket_status to completed
                    } else {
                        DB::table('tickets')
                            ->where('id',$request->id)
                            ->update(['request_status_id'=>4]);
                    }
                    
                }
            }

        } else if ($request->request_status_id == 4) {

            $completed_units = DB::table('unit_ticket')
                ->where('ticket_id', $request->id)
                ->where('request_status_id',4)->get();

            foreach($completed_units as $unit){
                DB::table('units')
                    ->where('id',$unit->unit_id)
                    ->update(['unit_status_id'=>1]);
            }

        }

         return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
