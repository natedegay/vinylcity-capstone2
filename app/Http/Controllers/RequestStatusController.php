<?php

namespace App\Http\Controllers;

use App\requestStatus;
use Illuminate\Http\Request;

class RequestStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\requestStatus  $requestStatus
     * @return \Illuminate\Http\Response
     */
    public function show(requestStatus $requestStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\requestStatus  $requestStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(requestStatus $requestStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\requestStatus  $requestStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, requestStatus $requestStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\requestStatus  $requestStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(requestStatus $requestStatus)
    {
        //
    }
}
