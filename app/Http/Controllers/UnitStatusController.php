<?php

namespace App\Http\Controllers;

use App\unitStatus;
use Illuminate\Http\Request;

class UnitStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\unitStatus  $unitStatus
     * @return \Illuminate\Http\Response
     */
    public function show(unitStatus $unitStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\unitStatus  $unitStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(unitStatus $unitStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\unitStatus  $unitStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, unitStatus $unitStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\unitStatus  $unitStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(unitStatus $unitStatus)
    {
        //
    }
}
