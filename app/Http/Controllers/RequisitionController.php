<?php

namespace App\Http\Controllers;

use App\Requisition;
use App\Unit;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;

class RequisitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->has('request')) {
            
            $units = Unit::find(array_keys(session('request')));
            return view('requisitions.index')->with('units',$units);
        } else {
            return view('requisitions.index');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function show(Requisition $requisition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function edit(Requisition $requisition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {

        // validate quantity input
        $request->validate([
            'quantity' => 'required|integer|min:1',
            'unit_status_id' => 'required|integer'
        ]);

        $quantity = $request->quantity;

        if($request->unit_status_id == 1){
            // store to session only if available
            $request->session()->put("request.$id",$quantity);
        }
        // dd(session('request'));
        return redirect( route('requisition.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Requisition  $requisition
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        session()->forget("request.$id");
        Toastr::warning('Removed record from request');

        // check if request form is empty
        if (empty(session('request'))) {
            session()->forget('request');
        }

        return back();
    }

    public function clear()
    {
        session()->forget('request');
        return back();
    }
}
