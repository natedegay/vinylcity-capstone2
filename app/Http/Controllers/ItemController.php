<?php

namespace App\Http\Controllers;

use App\Item;
use App\Unit;
use App\Status;
use App\unitStatus;
use Illuminate\Http\Request;
use DB;
use Str;
use Brian2694\Toastr\Facades\Toastr;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $items = Item::all()->sortBy('name');

        // store total units and available units per item
        foreach($items as $item){
            $item->total = DB::table('units')
                ->where('item_id',$item->id)
                ->whereNull('deleted_at')
                ->count();

            $item->available = DB::table('units')
                ->where('item_id',$item->id)
                ->where('unit_status_id',1)
                ->whereNull('deleted_at')
                ->count();
            $item->save();
        }
        
        return view('items.index')->with('items',$items);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // authenticate 
        $this-> authorize('create',Item::class);

        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        // authenticate 
        $this-> authorize('create',Item::class);

        // validate passed data
        $validatedData = $request->validate([
            'name' => 'required|unique:items,name',
        ]);

        $item = new Item($validatedData);

        $item->save();

        Toastr::success('Genre added successfully');
        return redirect( route('items.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {

       $ctrl_code = strtoupper(Str::random(6));
       $item_code = strtoupper(Str::substr($item->name,0,3));
        return view('items.show')
            ->with('item',$item)
            ->with('unit_statuses',unitStatus::all())
            ->with('units',Unit::all()->where('item_id',$item->id))
            ->with('ctrl_code',$ctrl_code)
            ->with('item_code',$item_code);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {   
        // authenticate 
        $this-> authorize('create',Item::class);

        return view('items.edit')->with('item',$item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        // authenticate 
        $this-> authorize('update',Item::class);

        $validatedData = $request->validate([
            'name' => 'required|unique:items,name'
        ]);

        $item->update($validatedData);

        $item->save();

        Toastr::success('Genre edited successfully');
        return redirect( route('items.show', $item->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {   
        // authenticate 
        $this-> authorize('delete',Item::class);

        $item->delete();

        Toastr::warning('Genre deleted successfully');
        return redirect( route('items.index'));
    }
}
