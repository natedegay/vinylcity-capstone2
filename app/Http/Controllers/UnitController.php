<?php

namespace App\Http\Controllers;

use App\Unit;
use App\Item;
use App\unitStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Str;
use DB;
use Brian2694\Toastr\Facades\Toastr;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::all();

        
        return view('units.index')->with('units',$units)->with('unit_statuses',unitStatus::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // authenticate
        $this->authorize('create', Unit::class);

        return view('units.create')->with('items', Item::all())->with('unit_statuses',unitStatus::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // authenticate
        $this ->authorize('create',Unit::class);

        // validate passed data
        $validatedData = $request->validate([
            'name' => 'required|string|unique:units,name',
            'control_code' => 'required',
            'item_id' => 'required',
            'unit_status_id' => 'required',
            'image' => 'required|image',
        ]);



        // save image after validation
        $path = $request->file('image')->store('images','s3');

        Storage::disk('s3')->setVisibility($path,'public');

         $url = Storage::disk('s3')->url($path);


        // create unit entry
        $unit = new Unit($validatedData);
        $unit->image = $url;

        // append control code to item code
        $unit->control_code = 
            strtoupper(Str::substr($unit->item->name,0,3)) 
            . "-" .
            $unit->control_code;

        // save to db
        $unit->save();

        Toastr::success('Record added successfully');

        return redirect( route('items.show',$unit->item_id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        return view('units.show')->with('unit',$unit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        // authenticate
        $this->authorize('create',Unit::class);

        return view('units.edit')->with('unit',$unit)->with('items', Item::all())->with('unit_statuses',unitStatus::all());;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {   
        $this->authorize('update',Unit::class);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'item_id' => 'required',
            'unit_status_id' => 'required',
            'image' => 'image',
        ]);

        $unit->update($validatedData);

        // Check if there is an image to be uploaded
        if($request->hasFile('image')) {
            $path = $request->file('image')->store('images','s3');

            Storage::disk('s3')->setVisibility($path,'public');

            $url = Storage::disk('s3')->url($path);
            $unit->image = $url;
        }

        $unit->save();

        Toastr::success('Record edited successfully');

        return redirect( route('units.show', $unit->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unit $unit)
    {
        // authorize
        $this->authorize('delete',Unit::class);
        
        $unit->delete();

        Toastr::warning('Record deleted successfully');
        return redirect( route('units.index'));
    }
}
