<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
     protected $fillable = ['date_needed','date_return'];

     public function request_status()
     {
     	return $this->belongsTo('App\requestStatus');
     }

     public function user()
     {
     	return $this->belongsTo('App\User');
     }

     public function units()
     {
     	return $this->belongsToMany('App\Unit','unit_ticket')
     		->withTimestamps();
     }
}
