<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class requestStatus extends Model
{
    public function tickets()
    {
    	return $this->hasMany('App\Ticket');
    }
}
