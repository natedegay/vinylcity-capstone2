<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Unit extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['name','image','control_code','item_id','unit_status_id'];

    public function item(){

    	return $this->belongsTo('App\Item');
   	}

   	public function unit_status(){

    	return $this->belongsTo('App\unitStatus');
   	}


}
