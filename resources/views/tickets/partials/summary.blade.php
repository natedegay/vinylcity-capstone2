{{-- table start --}} 
<div class="table-responsive">

	<table class="table table-borderless table-hover">
		{{-- ticket code --}}
		<tr>
			<td>Ticket Code</td>
			<td>{{ $ticket->ticket_code}}</td>
		</tr>
		{{-- ticket code end --}}
		
		{{-- Customer name start --}}
		<tr>
			<td>Customer</td>
			<td>{{ $ticket->user->name}}</td>
		</tr>
		{{-- Customer name end --}}

		{{-- Date requested start --}}
		<tr>
			<td>Date Requested</td>
			<td>{{ date("M d,Y - h:m:s",strtotime($ticket->created_at)) }}</td>
		</tr>
		{{-- Date requested end --}}

		{{-- Ticket status start --}}
		<tr>
			<td>Status</td>
			<td>
				<span 
					class="badge badge-pill
					@if($ticket->request_status_id == 1)
						badge-success
					@elseif($ticket->request_status_id == 2)
						badge-warning
					@elseif($ticket->request_status_id == 3)
						badge-danger
					@elseif($ticket->request_status_id == 4)
						badge-dark
					@endif
					"

				>
					{{ $ticket->request_status->name}}
				</span>
				{{-- hide if normal user --}}
				@can('isAdmin')
					@include('tickets.partials.edit-status')
				@endcan
			</td>
		</tr>
		{{-- Ticket status end --}}

		{{-- Date needed start --}}
		<tr>
			<td>Date Needed</td>
			<td>{{ date("M d,Y",strtotime($ticket->date_needed)) }}</td>
		</tr>
		{{-- Date needed end --}}

		{{-- Date to return start --}}
		<tr>
			<td>Date to be Returned</td>
			<td>{{ date("M d,Y",strtotime($ticket->date_return)) }}</td>
		</tr>
		{{-- Date to return end --}}		

	</table>
</div>
@include('tickets.partials.units-table')
{{-- table end			 --}}