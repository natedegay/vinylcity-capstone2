<form action="{{ route('tickets.update', $ticket->id)}}" method="post" class=" border p-3">
	@csrf
	@method("PUT")
	<label for="request_status_id">Edit:</label>
	<input type="hidden" name="id" id="id" value="{{ $ticket->id}}">
	<select name="request_status_id" id="request_status_id" class="form-control form-control-sm">
		@foreach($request_statuses as $request_status)
			<option value="{{$request_status->id}}"
				{{ $request_status->id === $ticket->request_status_id ? "selected" : ""}}
			>{{$request_status->name}}</option>
		@endforeach
	</select>
	<button class="btn btn-sm btn-outline-primary my-1">Edit Request Status</button>
</form>