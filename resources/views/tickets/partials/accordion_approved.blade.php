<div class="accordion" id="accordionExample">
	
	@foreach($approved_tickets as $ticket)
		{{-- ticket card start --}}
		<div class="card">
			<div class="card-header" id="headingOne">
				<h2 class="mb-0">
					<button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{$ticket->id}}" aria-expanded="true" aria-controls="collapseOne">
						{{ $ticket->ticket_code }}  
						<span 
							class="badge badge-primary
							@if($ticket->request_status_id == 1)
								badge-success
							@elseif($ticket->request_status_id == 2)
								badge-warning
							@elseif($ticket->request_status_id == 3)
								badge-danger
							@elseif($ticket->request_status_id == 4)
								badge-dark
							@endif
						"
						>
							{{ $ticket->request_status->name}}
						</span>
					</button>
				</h2>
			</div>

			<div id="collapse{{$ticket->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
				<div class="card-body">

					@include('tickets.partials.summary')
				</div>
			</div>
		</div>
		{{-- ticket card start --}}
	@endforeach

</div>