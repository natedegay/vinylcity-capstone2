<div class="row">
	<div class="col-12">
		{{-- table start --}}
		<div class="table-responsive">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Genre</th>
						<th>Record</th>
						<th>Record No</th>
						<th>Current Record Status</th>
					</tr>
				</thead>
				<tbody>
					{{-- unit row start --}}
					@foreach($ticket->units as $unit)
						<tr>
							<td>{{ $unit->item->name }}</td>
							<td>{{ $unit->name }}</td>
							<td>{{ $unit->control_code}}</td>
							<td>
								<badge 
									class="badge badge-success
									@if( $unit->unit_status_id == 1)
									badge-success
									@elseif( $unit->unit_status_id ==2)
									badge-danger
									@elseif( $unit->unit_status_id==3)
									badge-warning
									@endif
									"
								>	
									{{ $unit->unit_status->name }}				
								</badge>
							</td>
						</tr>
					@endforeach
					{{-- unit row end --}}
				</tbody>
			</table>
		</div>
		{{-- table end --}}
	</div>
</div>