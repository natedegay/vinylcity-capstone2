@extends('layouts.app')

@section('title','| My Requests')
@section('content')
	
	<div class="container my-5">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					My Requests
				</h1>
			</div>
		</div>
		<div class="row">
			
			{{-- start of left menu --}}
			<div class="col-4 col-lg-3">
					<!-- List group -->
					<div class="list-group" id="itemlist" role="tablist">
						<a class="list-group-item list-group-item-action active" href="#list-all" role="tab" data-toggle="list">All Tickets</a>
						
						<a class="list-group-item list-group-item-action" href="#list-approved" role="tab" data-toggle="list">Approved</a>

						<a class="list-group-item list-group-item-action" href="#list-pending" data-toggle="list">Pending</a>

						<a class="list-group-item list-group-item-action" href="#list-declined" data-toggle="list">Declined</a>

						<a class="list-group-item list-group-item-action" href="#list-completed" data-toggle="list">Completed</a>
					</div>
			</div>
			{{-- end of left menu --}}

			{{-- start of right content --}}
			<div class="col-8 col-lg-9">
				
				{{-- tickets section start --}}
				<div class="tab-content" id="nav-tabContent">
					<div class="tab-pane show active" id="list-all" role="tabpanel" aria-labelledby="list-home-list">
						@include('tickets.partials.accordion_all')
					</div>
					<div class="tab-pane" id="list-approved" role="tabpanel" aria-labelledby="list-profile-list">
						@include('tickets.partials.accordion_approved')
					</div>
					<div class="tab-pane" id="list-pending" role="tabpanel" aria-labelledby="list-messages-list">
						@include('tickets.partials.accordion_pending')
					</div>
					<div class="tab-pane" id="list-declined" role="tabpanel" aria-labelledby="list-settings-list">
						@include('tickets.partials.accordion_declined')
					</div>
					<div class="tab-pane" id="list-completed" role="tabpanel" aria-labelledby="list-settings-list">
						@include('tickets.partials.accordion_completed')
					</div>
				</div>

				{{-- tickets section end --}}

			</div>
			{{-- end right content --}}

		</div>

	</div>

@endsection