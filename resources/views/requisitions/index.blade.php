@extends('layouts.app')

@section('title','| Request')
@section('content')

	<div class="container my-5">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Requisitions
				</h1>
			</div>
		</div>

		@if(!Session::has('request'))
			{{-- alert start --}}
			<div class="alert alert-info alert-dismissible fade show text-center" role="alert">
				<button type="button" class="close" data-dismiss="alert">
					<span>&times;</span>
				</button>
				<strong>No requests to show</strong>
			</div>
		@else
		<div class="row">
			<div class="col-12">
				{{-- start request form --}}
				<div class="table-responsive">
					<table class="table table-hover text-center">
						{{-- start request table heading --}}
						<thead>
							<tr>
								<th>Genre</th>
								<th>Record</th>
								<th>Record No</th>
								<th>Current Status</th>
								<th>Action</th>
							</tr>
						</thead>
						{{-- end request table heading --}}
						{{-- start request table content --}}
						<tbody>
							@foreach($units as $unit)
								{{-- start unit row --}}
								<tr>
									<td>{{ $unit->item->name}}</td>
									<td>{{ $unit->name }}</td>
									<td>{{ $unit->control_code}}</td>
									<td><span class="badge badge-success">{{$unit->unit_status->name}}</span></td>
									<td>
										<form action="{{ route('requisition.destroy',$unit->id)}}" method="POST">
											@csrf
											@method('DELETE')
											<button class="btn btn-sm btn-outline-danger">Remove</button>
										</form>
									</td>
								</tr>
								{{-- end unit row --}}
							@endforeach
						</tbody>
						{{-- end request table content --}}
					</table>
				</div>
				{{-- end request form --}}
				{{-- start date required --}}
				<div class="row">
					<div class="col-12 col-sm-8 mx-auto">
						<form action="{{ route('tickets.store')}}" class="mb-5" method="POST">
							@csrf
							<div class="row align-items-center">
								{{-- start date needed --}}
								<div class="col align-self-start">
									<div class="form-group">
										<label for="date_needed">Date needed</label>
										<input required type="date"  name="date_needed" id="date_needed" class="form-control" min="<?= date("Y-m-d") ?>">
										<small id="dateNeeded" class="text-muted">Date when you need the record</small>
									</div>
								</div>
								{{-- end date needed --}}
								{{-- start date returned --}}
								<div class="col align-self-start">
									<div class="form-group">
										<label for="date_return">Date to return</label>
										<input required type="date" name="date_return" id="date_return" class="form-control" min="<?= date("Y-m-d") ?>">
										<small id="dateReturn" class="text-muted">Date to return the record</small>
									</div>
								</div>
								{{-- end date returned --}}
							</div>
							<button class="btn btn-primary">Submit</button>
						</form>
					</div>
				</div>
				{{-- end date required --}}
				<hr>
				<form action="{{ route('requisition.clear')}}" class="my-5" method="POST">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger">Clear Request Form</button>
				</form>
			</div>
		</div>
		@endif
		<script>
			@if(count($errors) > 0)
				@foreach($errors->all() as $error)
					toastr.error("{{ $error }}");
				@endforeach
			@endif
		</script>
	</div>

@endsection