@extends('layouts.app')

@section('title','| Records')
@section('content')
	<div class="container py-4">
		<div class="row">
			<div class="col-12">
				<div class="products-wrapper mt-3">
					<div class="row row-small-gutter">
						@foreach($units as $unit)
							@include('units.partials.card')
						@endforeach
						
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection