@extends('layouts.app')

@section('content')
	{{-- add unit section start --}}
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Add Record
				</h1>
			</div>
		</div>
		
		{{-- add unit form start --}}
		<div class="row">
			<div class="col-12 col-md-10 col-lg-6 mx-auto">
				
				<form action="{{ route('units.store')}}" method="post" enctype="multipart/form-data" class="form-group" >
					@csrf
					<label for="name" class="col-form-label">Unit Name</label>
					<input type="text" name="name" id="name">

					<label for="control_code" class="col-form-label">Control Code</label>
					<input type="text" name="control_code" id="control_code">

					<label for="image" class="col-form-label">Image</label>
					<input type="file" name="image" id="image">

					<label for="item_id" class="col-form-label">Item</label>
					<select name="item_id" id="item_id">
						@foreach($items as $item)
							<option value="{{ $item->id}}">{{ $item->name}}</option>
						@endforeach
					</select>

					<label for="unit_status_id" class="col-form-label">Unit Status</label>
					
					<select name="unit_status_id" id="unit_status_id">
						@foreach($unit_statuses as $unit_status)
							<option value="{{ $unit_status->id}}">{{ $unit_status->name}}</option>
						@endforeach
					</select>
					
					<button type="submit">Submit</button>

				</form>
			</div>
		</div>
		{{-- end add unit form --}}
	</div>
	{{-- end unit section --}}
@endsection