<div class="col-12 col-sm-6 col-md-4 col-lg-4 mx-auto">
    <div class="card card-product text-left my-2">
        <div class="card-body">
            <h6 class="text-left"> 
                    <span 
                        class="badge badge-pill d-inline-block
                            @if($unit->item_id ==1)
                                badge-jazz
                            @elseif($unit->item_id ==2)
                                badge-indie
                            @elseif($unit->item_id ==3)
                                badge-hiphop
                            @elseif($unit->item_id ==4)
                                badge-rock
                            @else
                                badge-secondary
                            @endif
                        "
                    >
                        {{ $unit->item->name}}
                    </span>
                    <span 
                        class="badge badge-pill d-inline-block
                            @if( $unit->unit_status_id == 1)
                                badge-success
                            @elseif( $unit->unit_status_id ==2)
                                badge-danger
                            @elseif( $unit->unit_status_id==3)
                                badge-warning
                            @endif
                        "
                    >
                        {{ $unit->unit_status->name}}
                    </span>
            </h6>


            <a href="{{ route('units.show',$unit->id)}}">
                <img src="{{$unit->image}}" class="img-fluid">
            </a>
            <div class="card-text text-center">
                <p class="card-text text-left">
                    Record No {{$unit->control_code}} 
                </p>
                <h4 class="album-title font-weight-bold text-left">{{$unit->name}}
                </h4>
                <hr>
                <div class="btn-group btn-group-sm">
                    
                    <form action="{{ route('requisition.update',$unit->id)}}" method="post" class="d-inline-block">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="quantity" value="1" >
                                <input type="hidden" name="unit_status_id" value="{{ $unit->unit_status_id}}">
                                <button class="btn btn-sm btn-dark btn-request"
                                    @if($unit->unit_status_id !== 1)
                                    disabled
                                    @else
                                    ""
                                    @endif
                                 >
                                    <span class="request">
                                        @if($unit->unit_status_id == 1) 
                                            Request
                                        @else
                                            Unavailable
                                        @endif
                                    </span>
                                </button>
                    </form> 
                    {{--hide if normal user --}}
                        @can('isAdmin')
            
                            <a href = "{{ route( 'units.edit', $unit->id ) }}" class="btn btn-sm btn-secondary">
                              <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                              </svg>
                            </a> 
         
         
                          <form action="{{ route('units.destroy',$unit->id)}}" method="post" class="d-inline-block">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-sm btn-danger">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                  <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                  <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                            </button>
                          </form>
  
                        @endcan
                </div>
            </div>
        </div>
    </div>
</div>
