@extends('layouts.app')

@section('content')
	{{-- edit unit section start --}}
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1 class="text-center">
					Edit <strong>{{ $unit->name}}</strong>
				</h1>
			</div>
		</div>
		
		{{-- edit unit form start --}}
		<div class="row">
			<div class="col-12 col-md-10 col-lg-6 mx-auto d-flex justify-content-center">
				
					<div class="my-2 add-record">

						<form action="{{ route('units.update',$unit->id)}}" method="post" enctype="multipart/form-data" >
							@csrf
							@method('PUT')
							
							{{-- name --}}
							<div class="form-group">
								<label for="name">Record Name</label>
								<input type="text" name="name" id="name" class="form-control" value="{{ $unit->name}}">
							</div>

							{{-- control-code --}}
							<div class="form-group">
								<label for="control_code">Record Code</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">{{$unit->control_code}} 
									</div>
								</div>
							</div>

							{{-- image --}}
							<div class="form-group">
								<label for="image">Image</label>
								<input type="file" name="image" id="image" class="form-control-file">
							</div>
							
							{{-- item --}}
							<div class="form-group">
								<label for="item_id">Genre</label>
								<select name="item_id" id="item_id" class="form-control">
									@foreach($items as $item)
										<option value= "{{ $item->id}}"
											{{ $item->id === $unit->item_id  ? "selected" : "" }} 
											>
											{{ $item->name}}
										</option>
									@endforeach
								</select>
							</div>

							{{-- unit_status --}}
							<div class="form-group">
								<label for="unit_status_id">Record Status</label>
								<select name="unit_status_id" id="unit_status_id" class="form-control">
								@foreach($unit_statuses as $unit_status)
									<option value="{{ $unit_status->id}}"
										{{ $unit_status->id === $unit->unit_status_id ? "selected" : "" }}
										>
										{{ $unit_status->name}}
									</option>
								@endforeach
								</select>
								
							</div>
							
							<button class="btn btn-secondary w-100" type="submit">Submit</button>

						</form>
					</div>
					{{-- end create unit container--}}

				</form>
			</div>
		</div>
		{{-- end add unit form --}}
		<script>
			@if(count($errors) > 0)
				@foreach($errors->all() as $error)
					toastr.error("{{ $error }}");
				@endforeach
			@endif
		</script>
	</div>
	{{-- end unit section --}}
@endsection