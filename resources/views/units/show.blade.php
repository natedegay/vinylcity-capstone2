@extends('layouts.app')

@section('content')
	<div class="container my-5">
		<div class="row">
			@include('units.partials.card')
		</div>
	</div>
@endsection