@extends('layouts.app')

@section('title', '| Genres')
@section('content')
	<div class="container mt-5">
		<div class="row justify-content-center">
			<div class="col-12 ">
					<div class="container" id="allitems">
						@foreach($items as $item)
							@include('items.partials.card')
						@endforeach
					</div>
					@can('isAdmin')
					<div class="container" id="createitem">
						@include('items.partials.create-item')
					</div>
					@endcan
					<script>
						@if(count($errors) > 0)
							@foreach($errors->all() as $error)
									toastr.error("{{ $error }}");
							@endforeach
						@endif
						
					</script>
			</div>
			{{-- end right content --}}
		</div>
	</div>
@endsection