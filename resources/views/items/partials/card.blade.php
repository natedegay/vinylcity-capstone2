<div class="card item-card bg-dark text-white mb-5">
  <img class="card-img" src="https://images.pexels.com/photos/1374557/pexels-photo-1374557.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="Card image">
  <div class="card-img-overlay" id="cardimg">
    <h2 class="card-title"><strong>{{$item->name}}</strong></h2>
    <p class="card-text">Current Available: {{ $item->available}}/{{ $item->total}}</p>
  <div class="footer">

    {{--hide if normal user --}}
    @can('isAdmin')
     <a href = "{{ route( 'items.edit', $item->id ) }}" class="btn btn-lg btn-dark">
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
      </svg>
    </a>
      <form action="{{ route('items.destroy',$item->id)}}" method="post" class="d-inline-block">
        @csrf
        @method('DELETE')
        <button class="btn btn-lg btn-danger"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
          <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
        </svg></button>
      </form>
    @endcan
     <a href = "{{ route( 'items.show', $item->id ) }}" class="btn btn-lg btn-light">
      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-eye-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
      <path d="M10.5 8a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
      <path fill-rule="evenodd" d="M0 8s3-5.5 8-5.5S16 8 16 8s-3 5.5-8 5.5S0 8 0 8zm8 3.5a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7z"/>
     </svg>
    </a>
  </div>
  </div>
</div>


{{-- <div class="card bg-dark text-white mb-3" >
  <div class="card-body">
    <h4 class="card-title">{{$item->name}}</h4>
    Current Available: {{ $item->available}}/{{ $item->total}}
  </div>
  <div class="card-footer">
    
    <a href = "{{ route( 'items.show', $item->id ) }}" class="btn btn-lg btn-primary">View</a>
    {{ hide if normal user --}}
   {{--  @can('isAdmin')
      <a href = "{{ route( 'items.edit', $item->id ) }}" class="btn btn-lg btn-dark">Edit</a>
      <form action="{{ route('items.destroy',$item->id)}}" method="post" class="d-inline-block">
        @csrf
        @method('DELETE')
        <button class="btn btn-lg btn-danger">Delete</button>
      </form>
    @endcan
  </div>
</div> --}}




