<div class="card card-flip h-100">
	<div class="card-front text-white bg-dark">
		<div class="card-body text-center">
			<h3 class="card-title">Add a New Genre</h3>
			<p class="card-text">Add new content from your awesome record collection!</p>
			<form action="{{ route('items.store') }}" method="post">
				@csrf
				<div class="input-group col-md-8">
					<input type="text" class="form-control" id="name" name="name" placeholder="Create New Genre">
					<div class="input-group-append">
						<button type="submit" class="btn btn-outline-success"><svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus-square-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
							<path fill-rule="evenodd" d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm6.5 4a.5.5 0 0 0-1 0v3.5H4a.5.5 0 0 0 0 1h3.5V12a.5.5 0 0 0 1 0V8.5H12a.5.5 0 0 0 0-1H8.5V4z"/>
						</svg></button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>





