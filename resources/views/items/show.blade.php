@extends('layouts.app')

@section('content')

	<div class="container my-5">
	<div class="row">
		<div class="col-12">
			<div class="card bg-dark text-white mb-5">
				<img class="card-img" src="https://images.pexels.com/photos/210764/pexels-photo-210764.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" alt="Card image">
				<div class="card-img-overlay" id="cardimg">
					<h2 class="card-title"><strong>{{$item->name}}</strong></h2>
					<p class="card-text">Current Available: {{ $item->available}}/{{ $item->total}}</p>
					    {{--hide if normal user --}}
					    @can('isAdmin')
					     	<a href = "{{ route( 'items.edit', $item->id ) }}" class="btn btn-lg btn-dark">
						      <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
						        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
						        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
						      </svg>
					    	</a>
					     	<form action="{{ route('items.destroy',$item->id)}}" method="post" class="d-inline-block">
					        @csrf
					        @method('DELETE')
					        <button class="btn btn-lg btn-danger">
					        	<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
					          		<path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
					          		<path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
					        	</svg>
					    	</button>
					      	</form>
					    @endcan
				</div>
			</div>
		</div>
	</div>
			
		<div class="row">
			{{-- start of item/unit left side --}}
			<div class="col-12 col-md-4 col-lg-3 mx-auto">
				{{-- hide if normal user --}}

				{{-- hide if normal user --}}
				@can('isAdmin')
					{{-- start of create unit container --}}
					<div class="my-2 add-record">
						<h5 class="text-center">Add Record</h5>

						<form action="{{ route('units.store')}}" method="post" enctype="multipart/form-data" >
							@csrf
							
							{{-- name --}}
							<div class="form-group">
								<label for="name">Record Name</label>
								<input type="text" name="name" id="name" class="form-control">
							</div>

							{{-- control-code --}}
							<div class="form-group">
								<label for="control_code">Record Code</label>
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">{{$item_code}} -
									</div>
									<input type="text" name="control_code" id="control_code" class="form-control" value="{{$ctrl_code}}">

								</div>
							</div>

							{{-- image --}}
							<div class="form-group">
								<label for="image">Image</label>
								<input type="file" name="image" id="image" class="form-control-file">
							</div>
							
							{{-- item --}}
							<div class="form-group">
								<label for="item_id">Genre</label>
								<select name="item_id" id="item_id" class="form-control">
										<option value="{{ $item->id}}">{{ $item->name}}</option>
								</select>
							</div>

							{{-- unit_status --}}
							<div class="form-group">
								<label for="unit_status_id">Record Status</label>
								<select name="unit_status_id" id="unit_status_id" class="form-control">
									@foreach($unit_statuses as $unit_status)
										<option value="{{ $unit_status->id}}">{{ $unit_status->name}}</option>
									@endforeach
								</select>
								
							</div>
							
							<button class="btn btn-secondary w-100" type="submit">Submit</button>

						</form>
					</div>
					{{-- end create unit container --}}
				@endcan
			</div>
			{{-- end of item/unit left side --}}
			
			{{-- start of show units right side --}}
			<div class="col-12 col-md-8 col-lg-9">
				<div class="row">
					<div class="products-wrapper mt-3">
					<div class="row row-small-gutter">
						@foreach($units as $unit)
							{{-- {{ dd($unit->name)}} --}}
							 @include('units.partials.card')
						@endforeach
					</div>
				</div>
				</div>
				
			</div>
			{{-- end of show units right side --}}
		</div>
		<script>
			@if(count($errors) > 0)
				@foreach($errors->all() as $error)
					toastr.error("{{ $error }}");
				@endforeach
			@endif
		</script>
	</div>


@endsection