@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<div class="my-3">
				<a href="{{ route('items.show',$item->id)}}">View Item</a>
			</div>
			<h3> Edit <strong>{{ $item->name }}</strong></h3>
			<div class="my-3">
				<form action="{{ route('items.update',$item->id) }}" method="post">
					@csrf
					@method('PUT')
					<div class="input-group">
						<input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}">
						
							<script>
								@if(count($errors) > 0)
									@foreach($errors->all() as $error)
										toastr.error("{{ $error }}");
									@endforeach
								@endif
							</script>
			
						<div class="input-group-append">
							<button type="submit" class="btn btn-outline-success">Edit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
